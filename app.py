import docker
import json
import logging
import os
import subprocess
import socket
import shutil
import time
from flask import Flask, request, jsonify
from werkzeug.utils import secure_filename
from threading import Thread

#Folder that must be in the server first before running this app, detailed information about this folder can be found in README.MD at Initialization Section number 7.
PRODUCT_REQUIREMENTS_DIRECTORY = "/home/hafiyyan94_gmail_com/dio/SPLE_Products"
PRODUCT_REQUIREMENTS_FOLDER_STATIC =  os.path.join(
    PRODUCT_REQUIREMENTS_DIRECTORY,"product_requirements/static")
PRODUCT_REQUIREMENTS_FOLDER_UPLOAD =  os.path.join(
    PRODUCT_REQUIREMENTS_DIRECTORY,"product_requirements/upload")
PRODUCT_REQUIREMENTS_FOLDER_NODE_MODULES_ANGULAR =os.path.join(
    PRODUCT_REQUIREMENTS_DIRECTORY,"product_requirements/angular_frontend/node_modules")
PRODUCT_REQUIREMENTS_FILE_YARN_ANGULAR =os.path.join(
    PRODUCT_REQUIREMENTS_DIRECTORY,"product_requirements/angular_frontend/yarn.lock")
PRODUCT_REQUIREMENTS_FOLDER_NODE_MODULES_REACT =os.path.join(
    PRODUCT_REQUIREMENTS_DIRECTORY,"product_requirements/react_frontend/node_modules")
PRODUCT_REQUIREMENTS_FILE_YARN_REACT =os.path.join(
    PRODUCT_REQUIREMENTS_DIRECTORY,"product_requirements/react_frontend/yarn.lock")

#Generated folder that save product static file and nginx configuration
SPLE_PRODUCTS_DIRECTORY = os.path.join(os.path.expanduser("~"), "SPLE_Products")
STATIC_PRODUCT_FOLDER = os.path.join(SPLE_PRODUCTS_DIRECTORY, "products_static")
NGINX_CONF = os.path.join(SPLE_PRODUCTS_DIRECTORY, "nginx_conf")
NGINX_CONF_PROD = os.path.join(SPLE_PRODUCTS_DIRECTORY, "nginx_conf_prod")
NGINX_LOGS = os.path.join(SPLE_PRODUCTS_DIRECTORY, "logs_nginx")

REPOSITORY_DOCKER = "test-dio.tk" #Change this to domain where you run Docker Registry for SPLE Products Docker Image

app = Flask(__name__)
app.config['SPLE_PRODUCTS_DIRECTORY'] = SPLE_PRODUCTS_DIRECTORY
app.config['PRODUCT_REQUIREMENTS_FOLDER_STATIC'] = PRODUCT_REQUIREMENTS_FOLDER_STATIC
app.config['PRODUCT_REQUIREMENTS_FOLDER_UPLOAD'] = PRODUCT_REQUIREMENTS_FOLDER_UPLOAD
app.config['PRODUCT_REQUIREMENTS_FOLDER_NODE_MODULES_ANGULAR'] = PRODUCT_REQUIREMENTS_FOLDER_NODE_MODULES_ANGULAR
app.config['PRODUCT_REQUIREMENTS_FILE_YARN_ANGULAR'] = PRODUCT_REQUIREMENTS_FILE_YARN_ANGULAR
app.config['PRODUCT_REQUIREMENTS_FOLDER_NODE_MODULES_REACT'] = PRODUCT_REQUIREMENTS_FOLDER_NODE_MODULES_REACT
app.config['PRODUCT_REQUIREMENTS_FILE_YARN_REACT'] = PRODUCT_REQUIREMENTS_FILE_YARN_REACT
app.config['STATIC_PRODUCT_FOLDER'] = STATIC_PRODUCT_FOLDER
app.config['NGINX_CONF'] = NGINX_CONF
app.config['NGINX_CONF_PROD'] = NGINX_CONF_PROD
app.config['NGINX_LOGS'] = NGINX_LOGS
app.config['REPOSITORY_DOCKER'] = REPOSITORY_DOCKER

logging.basicConfig(filename='flask.log', level=logging.DEBUG, format = '%(asctime)s %(levelname)s %(name)s %(threadName)s : %(message)s')

if not os.path.exists(app.config['SPLE_PRODUCTS_DIRECTORY']):
    os.makedirs(app.config['SPLE_PRODUCTS_DIRECTORY'])

if not os.path.exists(app.config['STATIC_PRODUCT_FOLDER']):
    os.makedirs(app.config['STATIC_PRODUCT_FOLDER'])

if not os.path.exists(app.config['NGINX_CONF']):
    os.makedirs(app.config['NGINX_CONF'])

if not os.path.exists(app.config['NGINX_CONF_PROD']):
    os.makedirs(app.config['NGINX_CONF_PROD'])

if not os.path.exists(app.config['NGINX_LOGS']):
    os.makedirs(app.config['NGINX_LOGS'])

client = docker.from_env()

def restart_nginx():
    time.sleep(1)
    subprocess.run(["sudo", "service", "nginx", "restart"])

def start_container(product_name, nginx_port, frontend_engine):
    try:
        client.containers.get(product_name).stop()
        client.containers.get(product_name).remove()
    except:
        pass

    static_folder_path = os.path.join(app.config['STATIC_PRODUCT_FOLDER'], product_name)
    static_upload = os.path.join(static_folder_path, "upload")
    static_data_static = os.path.join(static_folder_path, "static")
    static_auth = os.path.join(static_folder_path, "auth.properties")
    static_config = os.path.join(static_folder_path, "config.properties")

    if frontend_engine == 'react':
        created_container = client.containers.get(client.containers.create(
            image = app.config['REPOSITORY_DOCKER'] + "/" + product_name, name=product_name, ports={str(nginx_port)+'/tcp':str(nginx_port)},
            volumes = {app.config['PRODUCT_REQUIREMENTS_FOLDER_NODE_MODULES_REACT']:{'bind': '/frontend/node_modules', 'mode': 'rw'},
                app.config['PRODUCT_REQUIREMENTS_FILE_YARN_REACT']:{'bind': '/frontend/yarn.lock', 'mode': 'rw'},
                static_upload:{'bind': '/admin/upload', 'mode': 'rw'}, 
                static_data_static:{'bind': '/admin/static', 'mode': 'rw'},
                static_auth:{'bind': '/backend/auth.properties', 'mode': 'rw'},
                static_config:{'bind': '/backend/config.properties', 'mode': 'rw'}}
        ).id)
    else :
        created_container = client.containers.get(client.containers.create(
            image = app.config['REPOSITORY_DOCKER'] + "/" + product_name, name=product_name, ports={str(nginx_port)+'/tcp':str(nginx_port)},
            volumes = {app.config['PRODUCT_REQUIREMENTS_FOLDER_NODE_MODULES_ANGULAR']:{'bind': '/frontend/node_modules', 'mode': 'rw'},
                app.config['PRODUCT_REQUIREMENTS_FILE_YARN_ANGULAR']:{'bind': '/frontend/yarn.lock', 'mode': 'rw'},
                static_upload:{'bind': '/admin/upload', 'mode': 'rw'}, 
                static_data_static:{'bind': '/admin/static', 'mode': 'rw'},
                static_auth:{'bind': '/backend/auth.properties', 'mode': 'rw'},
                static_config:{'bind': '/backend/config.properties', 'mode': 'rw'}}
        ).id)
    created_container.start()

    return True

@app.route('/dockerdeploy', methods=['POST'])
def docker_deploy():
    req_data = request.get_json()
    product_name = req_data['product_name']
    nginx_port = req_data['nginx_port']
    frontend_engine = req_data['frontend_engine']

    with open(os.path.join(app.config['STATIC_PRODUCT_FOLDER'], product_name, product_name + '.json'), 'w') as json_file:
        json.dump(req_data, json_file)

    client.images.pull(app.config['REPOSITORY_DOCKER'] + "/"+ product_name)
    running_container = start_container(product_name, nginx_port, frontend_engine)

    if running_container:
        return jsonify(status="success")
    else:
        return jsonify(status="failed"), 500
    
@app.route('/nginxport', methods=['POST'])
def get_port():
    nginx_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    nginx_socket.bind(("", 0))
    nginx_port = nginx_socket.getsockname()[1]
    nginx_socket.close

    return jsonify(port=str(nginx_port)) 

@app.route('/setupserver', methods=['POST'])
def setup_server():
    if 'nginx_conf' not in request.files:
        return 'There is no file in body request ...'

    json_file = request.files['maintenance_mode']
    json_file.save('data.txt')

    with open('data.txt') as json_file:
            data = json.load(json_file)

    nginx_file = request.files['nginx_conf']
    nginx_file_mnt = request.files['nginx_conf_mnt']
    auth_file = request.files['auth']
    config_file = request.files['config']
    html_404_page = request.files['404_page']
    html_maintenance_page = request.files['maintenance_page']
    product_name = nginx_file.filename.split(".")[0]
    nginx_name = secure_filename(nginx_file.filename)
    nginx_name_mnt = secure_filename(nginx_file_mnt.filename)

    nginx_file.save(os.path.join(app.config['NGINX_CONF'], nginx_name))
    nginx_file_mnt.save(os.path.join(app.config['NGINX_CONF'], nginx_name_mnt))

    if data['maintenance_mode']:
        shutil.copy(os.path.join(app.config['NGINX_CONF'], nginx_name_mnt), os.path.join(app.config['NGINX_CONF_PROD'], product_name + "PROD.conf"))
    else :
        shutil.copy(os.path.join(app.config['NGINX_CONF'], nginx_name), os.path.join(app.config['NGINX_CONF_PROD'], product_name + "PROD.conf"))

    if not os.path.exists(os.path.join(app.config['NGINX_LOGS'], product_name.lower())):
        os.makedirs(os.path.join(app.config['NGINX_LOGS'], product_name.lower()))

    static_folder_path = os.path.join(app.config['STATIC_PRODUCT_FOLDER'], product_name.lower())

    if not os.path.exists(static_folder_path):
        os.makedirs(static_folder_path)

    file_auth_name = secure_filename(auth_file.filename)
    file_config_name = secure_filename(config_file.filename)
    auth_file.save(os.path.join(static_folder_path, file_auth_name))
    config_file.save(os.path.join(static_folder_path, file_config_name))
    html_404_page.save(os.path.join(static_folder_path, '404.html'))
    html_maintenance_page.save(os.path.join(static_folder_path, 'maintenance.html'))

    subprocess.run(["cp", "-r", app.config['PRODUCT_REQUIREMENTS_FOLDER_STATIC'], static_folder_path]) 
    subprocess.run(["cp", "-r", app.config['PRODUCT_REQUIREMENTS_FOLDER_UPLOAD'], static_folder_path])

    if os.path.exists(os.path.join(app.config['NGINX_LOGS'], product_name.lower(), "nginx_proxy_access.log")):
        os.remove(os.path.join(app.config['NGINX_LOGS'], product_name.lower(), "nginx_proxy_access.log"))
    
    if os.path.exists(os.path.join(app.config['NGINX_LOGS'], product_name.lower(), "nginx_proxy_error.log")):
        os.remove(os.path.join(app.config['NGINX_LOGS'], product_name.lower(), "nginx_proxy_error.log"))

    subprocess.run(["touch", os.path.join(app.config['NGINX_LOGS'], product_name.lower(), "nginx_proxy_access.log")])
    subprocess.run(["touch", os.path.join(app.config['NGINX_LOGS'], product_name.lower(), "nginx_proxy_error.log")])

    thread = Thread(target=restart_nginx)
    thread.start()

    return jsonify(
        status = "success"
    )

@app.route('/refreshnginx', methods=['POST'])
def refresh_nginx():
    req_data = request.get_json()
    product_name = str(req_data['product_name']).replace(" ","")

    with open(os.path.join(app.config['STATIC_PRODUCT_FOLDER'], product_name.lower(), product_name.lower() + '.json')) as json_file:
        data = json.load(json_file)
    
    if req_data['server'] == '1':
        data['maintenance_mode'] = req_data['maintenance_mode']

    if req_data['server'] == '3':
        data['maintenance_mode'] = False

    with open(os.path.join(app.config['STATIC_PRODUCT_FOLDER'], product_name.lower(), product_name.lower() + '.json'), 'w') as json_file:
        json.dump(data, json_file   )

    try:
        if data['maintenance_mode']:
            new_nginx_conf = os.path.join(app.config["NGINX_CONF"], product_name + "MNT.conf")
            nginx_destination = os.path.join(app.config["NGINX_CONF_PROD"], product_name + "PROD.conf")
            shutil.copy(new_nginx_conf, nginx_destination)
        else:
            new_nginx_conf = os.path.join(app.config["NGINX_CONF"], product_name + ".conf")
            nginx_destination = os.path.join(app.config["NGINX_CONF_PROD"], product_name + "PROD.conf")
            shutil.copy(new_nginx_conf, nginx_destination)

        thread = Thread(target=restart_nginx)
        thread.start()

    except Exception as e:
            return jsonify(
                status = "failed",
                message = e
            )

    return jsonify(status = "success")

@app.route('/dockerstart', methods=['POST'])
def docker_start():
    req_data = request.get_json()

    product_name = req_data['product_name'].replace(" ","").lower()

    if not os.path.exists(os.path.join(app.config['STATIC_PRODUCT_FOLDER'], product_name)):
        return jsonify(status="failed"), 500

    with open(os.path.join(app.config['STATIC_PRODUCT_FOLDER'], product_name, product_name + '.json')) as json_file:
        data = json.load(json_file)

    try:
        #check if images or product is in runing server
        client.images.get(app.config['REPOSITORY_DOCKER'] + "/" + data['product_name'])
        start_container(data['product_name'], data['nginx_port'], data['frontend_engine'])
    except Exception as e:
        logging.error(e)
        return jsonify(status = "failed", message = "there is no container and image with that product name"), 500

    return jsonify(status = "success")

@app.route('/dockerstop', methods=['POST'])
def docker_stop():
    req_data = request.get_json()

    product_name = req_data['product_name'].replace(" ","").lower()

    try:
        client.containers.get(product_name).stop()
    except Exception as e:
        logging.error(e)
        return jsonify(status = "failed", message = "there is no container with that product name"), 500

    return jsonify(status = "success")

@app.route('/dockerdestroy', methods=['POST'])
def docker_delete():
    req_data = request.get_json()

    product_name = req_data['product_name'].replace(" ","")

    try:
        client.containers.get(product_name.lower()).stop()
        client.containers.get(product_name.lower()).remove()
        client.images.remove(app.config['REPOSITORY_DOCKER'] + "/" + product_name.lower())
        shutil.rmtree(os.path.join(app.config['STATIC_PRODUCT_FOLDER'], product_name.lower()))
        os.remove(os.path.join(app.config['NGINX_CONF'], product_name + ".conf"))
        os.remove(os.path.join(app.config['NGINX_CONF'], product_name + "MNT.conf"))
        os.remove(os.path.join(app.config['NGINX_CONF_PROD'], product_name + "PROD.conf"))
        shutil.rmtree()(os.path.join(app.config['NGINX_LOGS'], product_name))
    except Exception as e:
        logging.error(e)
        return jsonify(status = "failed", message = "there is no container and image with that product name"), 500
    
    return jsonify(status = "success")
        
if __name__ == "__main__":
    app.run(debug=True)